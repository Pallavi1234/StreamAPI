package apistream;


import java.security.spec.RSAOtherPrimeInfo;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.lang.System;
import java.util.Optional;
import java.util.stream.Collectors;


public class streamPract {

    public static void main(String[] args)
    {

    List<String> list = Arrays.asList("ABC", "Def", "d");
    System.out.println(list.stream().filter(i -> i.length()>2).collect(Collectors.toList()));

    List<String> list1 = Arrays.asList("USA","Japan", "France","Germany",  "India", "U.K.","Canada");
    System.out.println(list1.stream().map(String::toUpperCase).collect(Collectors.joining(",")));

    List<Integer> list2 = Arrays.asList(9, 10, 3, 4, 7, 3, 4);
    System.out.println(list2.stream().distinct().map(n -> n*n).collect(Collectors.toList()));

    Optional<Integer> maxnum = list2.stream().max(Integer::compareTo);
    System.out.println(maxnum);

    List<Integer> list3 = Arrays.asList(1,2,2,4,2,5);
    System.out.println(list3.stream().filter(i -> i==2).count());

    List<Integer> list4 = Arrays.asList(1,2,3,4,5,6,7,8);
    System.out.println(list4.stream().anyMatch(i -> i.equals(2)));

    System.out.println(list4.parallelStream().anyMatch(i -> i.equals(2)));

    }
}
